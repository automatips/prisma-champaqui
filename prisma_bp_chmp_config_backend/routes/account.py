from fastapi import APIRouter, Response, status
from persistence.db import conn
from models.account import accounts
from repositories.account import Account
account = APIRouter()

@account.get("/accounts", response_model=list[Account], tags=["accounts"])
def get_accounts():
    return conn.execute(accounts.select()).fetchall()

@account.post("/accounts", response_model=Account, tags=["accounts"])
def create_account(account:Account):
    new_account = {"Account_ID":account.Account_ID,"FIID":account.FIID, "FIID_Padre":account.FIID_Padre}
    conn.execute(accounts.insert().values(new_account))
    
@account.get("/accounts/{id}", response_model=Account, tags=["accounts"])
def get_account(id:int):
    return conn.execute(accounts.select().where(accounts.c.Account_ID == id)).first()

@account.delete("/accounts/{id}",status_code=status.HTTP_204_NO_CONTENT, tags=["accounts"])
def delete_account(id:int):
    conn.execute(accounts.delete().where(accounts.c.Account_ID == id))
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@account.put("/accounts/{id}", response_model=Account, tags=["accounts"])
def edit_account(id:int, account:Account):
    conn.execute(accounts.update().values(FIID=account.FIID,FIID_Padre= account.FIID_Padre).where(accounts.c.Account_ID == id)).first()