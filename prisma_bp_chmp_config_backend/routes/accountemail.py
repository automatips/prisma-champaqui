from fastapi import APIRouter, Response, status
from persistence.db import conn
from models.accountemail import accountemails
from repositories.accountemail import AccountEmail
accountemail = APIRouter()

@accountemail.get("/accountemails", response_model=list[AccountEmail], tags=["accountemails"])
def get_accountemails():
    return conn.execute(accountemails.select()).fetchall()

@accountemail.post("/accountemails", response_model=AccountEmail, tags=["accountemails"])
def create_accountemail(accountemail:AccountEmail):
    new_accountemail = {"AccountEmail_ID":accountemail.AccountEmail_ID,
                   "Account_ID":accountemail.Account_ID, 
                   "AccountEmailGroup_ID":accountemail.AccountEmailGroup_ID,
                   "Email_ID":accountemail.Email_ID,
                   "MailName":accountemail.MailName,
                   "MailAdress":accountemail.MailAdress
                   }
    conn.execute(accountemails.insert().values(new_accountemail))
    
@accountemail.get("/accountemails/{id}", response_model=AccountEmail, tags=["accountemails"])
def get_accountemail(id:int):
    return conn.execute(accountemails.select().where(accountemails.c.AccountEmail_ID == id)).first()

@accountemail.delete("/accountemails/{id}",status_code=status.HTTP_204_NO_CONTENT, tags=["accountemails"])
def delete_accountemail(id:int):
    conn.execute(accountemails.delete().where(accountemails.c.AccountEmail_ID == id))
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@accountemail.put("/accountemails/{id}", response_model=AccountEmail, tags=["accountemails"])
def edit_accountemail(id:int, accountemail:AccountEmail):
    conn.execute(accountemails.update().values(Account_ID=accountemail.Account_ID,
                                               AccountEmailGroup_ID=accountemail.AccountEmailGroup_ID,
                                               Email_ID=accountemail.Email_ID,
                                               MailName=accountemail.MailName,
                                               MailAdress=accountemail.MailAdress,
                                               ).where(accountemail.c.AccountEmail_ID == id)).first()