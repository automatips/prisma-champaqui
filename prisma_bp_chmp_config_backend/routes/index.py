from fastapi import APIRouter, Response, status
from persistence.db import conn
from models.account import accounts
from models.accountEmailGroup import accountEmailGroups
from models.accountFolder import accountFolders
from models.accountGroup import accountGroups
from models.allowedFilesFolder import allowedFilesFolders
from models.design import designs
from models.destination import destinations
from models.emailTemplate import emailTemplates
from models.folder import folders
from models.frequency import frequencies
from models.inputFile import inputFiles
from models.outputFile import outputFiles
from models.outputFilesDestination import outputFilesDestinations
from models.processType import processTypes
from models.rubro import rubros
from models.rule import rules
from models.ruleGroup import ruleGroups
from models.ruleGroupRuleParameter import ruleGroupRuleParameters
from models.ruleGroupsRule import ruleGroupsRules
from models.server import servers
from models.validationGroup import validationGroups
from models.validationRule import validationRules

from repositories.account import Account
from repositories.accountEmailGroup import AccountEmailGroup
from repositories.accountFolder import AccountFolder
from repositories.accountGroup import AccountGroup
from repositories.allowedFilesFolder import AllowedFilesFolder
from repositories.design import Design
from repositories.destination import Destination
from repositories.emailTemplate import EmailTemplate
from repositories.folder import Folder
from repositories.frequency import Frequency
from repositories.inputFile import InputFile
from repositories.outputFile import OutputFile
from repositories.outputFilesDestination import OutputFilesDestination
from repositories.processType import ProcessType
from repositories.rubro import Rubro
from repositories.rule import Rule
from repositories.ruleGroup import RuleGroup
from repositories.ruleGroupRuleParameter import RuleGroupRuleParameter
from repositories.ruleGroupsRule import RuleGroupsRule
from repositories.server import Server
from repositories.validationGroup import ValidationGroup
from repositories.validationRule import ValidationRule
index = APIRouter()

@index.get("/", tags=["index"])
def get_index():
    return status.HTTP_200_OK
