from fastapi import APIRouter, Response, status
from persistence.db import conn
from models.emailTemplate import emailTemplates
from repositories.emailTemplate import EmailTemplate
emailtemplate = APIRouter()

@emailtemplate.get("/emailtemplates", response_model=list[EmailTemplate], tags=["emailtemplates"])
def get_emailtemplates():
    return conn.execute(emailTemplates.select()).fetchall()

@emailtemplate.post("/emailtemplates", response_model=EmailTemplate, tags=["emailtemplates"])
def create_emailtemplate(emailTemplate:EmailTemplate):
    new_emailtemplate = {"EmailTemplate_ID":emailTemplate.EmailTemplate_ID,
                   "TemplateName":emailTemplate.TemplateName, 
                   "EmailSubject":emailTemplate.EmailSubject,
                   "EmailBodyTemplate":emailTemplate.EmailBodyTemplate
                   }
    conn.execute(emailTemplates.insert().values(new_emailtemplate))
    
@emailtemplate.get("/emailtemplates/{id}", response_model=EmailTemplate, tags=["emailtemplates"])
def get_emailtemplate(id:int):
    return conn.execute(emailTemplates.select().where(emailTemplates.c.EmailTemplate_ID == id)).first()

@emailtemplate.delete("/emailtemplates/{id}",status_code=status.HTTP_204_NO_CONTENT, tags=["emailtemplates"])
def delete_emailtemplate(id:int):
    conn.execute(emailTemplates.delete().where(emailTemplates.c.EmailTemplate_ID == id))
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@emailtemplate.put("/emailtemplates/{id}", response_model=EmailTemplate, tags=["emailtemplates"])
def edit_emailtemplate(id:int, emailTemplate:EmailTemplate):
    conn.execute(emailTemplates.update().values(TemplateName=emailTemplate.TemplateName,
                                                EmailSubject= emailTemplate.EmailSubject,
                                                EmailBodyTemplate= emailTemplate.EmailBodyTemplate
                                                ).where(emailTemplates.c.EmailTemplate_ID == id)).first()