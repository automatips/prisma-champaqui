from fastapi import APIRouter, Response, status
from persistence.db import conn
from models.accountEmailGroup import accountEmailGroups
from repositories.accountEmailGroup import AccountEmailGroup
accountemailgroup = APIRouter()

@accountemailgroup.get("/accountemailgroups", response_model=list[AccountEmailGroup], tags=["accountemailgroups"])
def get_accountemailgroups():
    return conn.execute(accountEmailGroups.select()).fetchall()

@accountemailgroup.post("/accountemailgroups", response_model=AccountEmailGroup, tags=["accountemailgroups"])
def create_accountemailgroup(accountemailgroup:AccountEmailGroup):
    new_accountemailgroup = {"AccountEmailGroup_ID":accountemailgroup.AccountEmailGroup_ID,
                   "Account_ID":accountemailgroup.Account_ID, 
                   "SubjectPrefix":accountemailgroup.SubjectPrefix,
                   "EmailTemplate_ID":accountemailgroup.EmailTemplate_ID,
                   }
    conn.execute(accountEmailGroups.insert().values(new_accountemailgroup))
    
@accountemailgroup.get("/accountemailgroups/{id}", response_model=AccountEmailGroup, tags=["accountemailgroups"])
def get_accountemailgroup(id:int):
    return conn.execute(accountemailgroup.select().where(accountemailgroup.c.AccountEmailGroup_ID == id)).first()

@accountemailgroup.delete("/accountemailgroups/{id}",status_code=status.HTTP_204_NO_CONTENT, tags=["accountemailgroups"])
def delete_accountemailgroup(id:int):
    conn.execute(accountEmailGroups.delete().where(accountemailgroup.c.AccountEmailGroup_ID == id))
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@accountemailgroup.put("/accountemailgroups/{id}", response_model=AccountEmailGroup, tags=["accountemailgroups"])
def edit_accountemailgroup(id:int, accountemailgroup:AccountEmailGroup):
    conn.execute(accountEmailGroups.update().values(Account_ID=accountemailgroup.Account_ID,
                                                    SubjectPrefix= accountemailgroup.SubjectPrefix,
                                                    EmailTemplate_ID= accountemailgroup.EmailTemplate_ID
                                                    ).where(accountemailgroup.c.AccountEmailGroup_ID == id)).first()