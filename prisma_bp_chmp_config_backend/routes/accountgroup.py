from fastapi import APIRouter, Response, status
from persistence.db import conn
from models.accountGroup import accountGroups
from repositories.accountGroup import AccountGroup
accountgroup = APIRouter()

@accountgroup.get("/accountgroups", response_model=list[AccountGroup], tags=["accountgroups"])
def get_accounts():
    return conn.execute(accountGroups.select()).fetchall()

@accountgroup.post("/accountgroups", response_model=AccountGroup, tags=["accountgroups"])
def create_account(accountgroup:AccountGroup):
    new_accountgroup = {"AccountGroup_ID":accountgroup.AccountGroup_ID,
                   "Account_ID":accountgroup.Account_ID,
                   "AccountEmailGroup_ID":accountgroup.AccountEmailGroup_ID,
                   "Email_ID":accountgroup.Email_ID,
                   "AccountGroupName":accountgroup.AccountGroupName
                   }
    conn.execute(accountGroups.insert().values(new_accountgroup))
    
@accountgroup.get("/accountgroups/{id}", response_model=AccountGroup, tags=["accountgroups"])
def get_accountgroup(id:int):
    return conn.execute(accountGroups.select().where(accountGroups.c.AccountGroup_ID == id)).first()

@accountgroup.delete("/accountgroups/{id}",status_code=status.HTTP_204_NO_CONTENT, tags=["accountgroups"])
def delete_account(id:int):
    conn.execute(accountGroups.delete().where(accountGroups.c.AccountGroup_ID == id))
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@accountgroup.put("/accountgroups/{id}", response_model=AccountGroup, tags=["accountgroups"])
def edit_accountgroup(id:int, accountgroup:AccountGroup):
    conn.execute(accountGroups.update().values(Account_ID= accountgroup.Account_ID,
                                               AccountEmailGroup_ID=accountgroup.AccountEmailGroup_ID,
                                               Email_ID=accountgroup.Email_ID,
                                               AccountGroupName=accountgroup.AccountGroupName
                                               ).where(accountGroups.c.AccountGroup_ID == id)).first()