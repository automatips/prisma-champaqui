from pydantic import BaseModel

class Frequency(BaseModel):
    Frequency_ID: int
    Frequency_Name: str