from pydantic import BaseModel

class AccountEmailGroup(BaseModel):
    AccountEmailGroup_ID: int
    Account_ID: int
    SubjectPrefix: int
    EmailTemplate_ID: int