from pydantic import BaseModel
from sqlalchemy.sql.expression import text

class AccountEmail(BaseModel):
    AccountEmail_ID: int
    Account_ID: int
    AccountEmailGroup_ID: int
    Email_ID: int
    MailName: str
    MailAdress: str