from pydantic import BaseModel

class Destination(BaseModel):
    Destination_ID: int
    Folder_ID: int
    MailConfiguration_ID: int