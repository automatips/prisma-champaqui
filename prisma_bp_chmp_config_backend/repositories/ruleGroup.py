from pydantic import BaseModel

class RuleGroup(BaseModel):
    RuleGroup_ID: int
    Name: str