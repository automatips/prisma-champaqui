from pydantic import BaseModel

class AccountFolder(BaseModel):
    AccountFolder_ID: int
    Account_ID: int
    Folder_ID: int
    Server_ID: str
    Path: str
    RuleGroup_ID: int