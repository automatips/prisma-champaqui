from pydantic import BaseModel

class ValidationRulesGroup(BaseModel):
    ValidationRulesGroup_Id: int
    ValidationGroup_ID: int
    ValidationRule_ID: int
    Order: int