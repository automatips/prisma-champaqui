from pydantic import BaseModel

class RuleGroupsRule(BaseModel):
    RuleGroupsRules_ID: int
    RuleGroup_ID: int
    Rule_Id: int
    Order: str