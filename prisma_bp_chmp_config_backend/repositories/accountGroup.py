from pydantic import BaseModel

class AccountGroup(BaseModel):
    AccountGroup_ID: int
    Account_ID: int
    AccountEmailGroup_ID: int
    Email_ID: int
    AccountGroupName: str