from pydantic import BaseModel

class Rule(BaseModel):
    Rule_ID: int
    ActionType: int
    URL: str
    Script: str