from pydantic import BaseModel

class OutputFile(BaseModel):
    OutputFile_ID: int
    Account_ID: int
    FIID: int
    OriginalName: int
    FileBasename: int
    DateFormat_ID: int
    FileExtension: int
    Refresh_ID: int
    Design_ID: int
    ProcessType_ID: int
    Encodding: int
    ShouldEncrypt: int
    ShouldZip: int