from pydantic import BaseModel

class EmailTemplate(BaseModel):
    EmailTemplate_ID: int
    TemplateName: str
    EmailSubject: str
    EmailBodyTemplate: str