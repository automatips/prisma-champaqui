from pydantic import BaseModel

class ValidationRule(BaseModel):
    ValidationRule_ID: int
    ActionType: int
    URL: str
    Script: str