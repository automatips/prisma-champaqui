from pydantic import BaseModel

class AllowedFilesFolder(BaseModel):
    AllowedFilesFolder_ID: int
    Account_ID: int
    Folder_ID: int
    AllowedFile_ID: int
    InputFile_ID: int
    OutputFile_ID: int