from pydantic import BaseModel

class ValidationGroup(BaseModel):
    ValidationGroup_ID: int
    Name: str