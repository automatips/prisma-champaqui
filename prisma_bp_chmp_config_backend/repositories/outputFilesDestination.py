from pydantic import BaseModel

class OutputFilesDestination(BaseModel):
    OutputFilesDestination_ID: int
    Account_ID: int
    OutputFile_ID: int
    Destination_ID: int