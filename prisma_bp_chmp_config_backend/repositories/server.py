from pydantic import BaseModel

class Server(BaseModel):
    Server_ID: str
    ServerName: str
    ServerIP: str