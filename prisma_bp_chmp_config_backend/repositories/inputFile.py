from pydantic import BaseModel

class InputFile(BaseModel):
    InputFile_ID: int
    Account_ID: int
    FIID: int
    Basename: int
    Refresh_ID: int
    Design_ID: int
    ProcessType_ID: int
    DateFormat_ID: int
    ValidationRulesGroup_ID: int
    Encodding: int
    CanEncrypt: int
    CanZip: int
    CanPadding: int
    Frequency_ID: int
    Rubro_ID: int
    Should_Notify_Delay: int
    Waiting_To_Notify_Days: int