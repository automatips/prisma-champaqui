from pydantic import BaseModel

class ProcessType(BaseModel):
    ProcessType_ID: int
    ProcessType_Name: str