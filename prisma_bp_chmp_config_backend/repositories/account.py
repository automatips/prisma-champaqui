from pydantic import BaseModel
from sqlalchemy.sql.expression import text

class Account(BaseModel):
    Account_ID: int
    FIID: str
    FIID_Padre: int