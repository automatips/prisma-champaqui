from pydantic import BaseModel
from sqlalchemy.sql.expression import text

class Folder(BaseModel):
    Folder_ID: int
    Server_ID: str
    Path: str