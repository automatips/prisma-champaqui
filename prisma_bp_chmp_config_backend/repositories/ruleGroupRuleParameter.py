from pydantic import BaseModel

class RuleGroupRuleParameter(BaseModel):
    RuleGroupRuleParameter_ID: int
    RuleGroup_ID: int
    Rule_ID: int
    Parameter_Order: int
    Parameter_Name: str
    Parameter_Value: str