from pydantic import BaseModel

class Design(BaseModel):
    Design_ID: int
    Design_Name: str
    Record_len: int
    Padding: int