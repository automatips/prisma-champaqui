from fastapi import FastAPI
from routes.index import index
from routes.account import account
from routes.accountemail import accountemail
from routes.accountemailgroup import accountemailgroup
from routes.accountgroup import accountgroup
from routes.emailtemplate import emailtemplate
from fastapi.middleware.cors import CORSMiddleware

origins = ["*"]

app = FastAPI(
    title="Champaquí API",
    description="Champaquí API",
    openapi_tags=[{
        "name":"Champaqui Project",
        "description":"PRISMA MP - Champaqui Project"
    }]
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(index)
app.include_router(account)
app.include_router(accountemail)
app.include_router(accountemailgroup)
app.include_router(accountgroup)
app.include_router(emailtemplate)