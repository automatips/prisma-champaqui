from sqlalchemy import *
from persistence.db import meta, engine

accountemails = Table("AccountEmails", meta, 
                 Column("AccountEmail_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("AccountEmailGroup_ID", Integer),
                 Column("Email_ID", Integer),
                 Column("MailName", Text),
                 Column("MailAdress", Text)
                 )

meta.create_all(engine)