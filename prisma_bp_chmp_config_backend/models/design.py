from sqlalchemy import *
from persistence.db import meta, engine

designs = Table("Designs", meta,
                 Column("Design_ID", Integer, primary_key=True),
                 Column("Design_Name", Text),
                 Column("Record_len", Integer),
                 Column("Padding", Integer)
                 )

meta.create_all(engine)