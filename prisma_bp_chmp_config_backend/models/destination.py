from sqlalchemy import *
from persistence.db import meta, engine

destinations = Table("Destinations", meta, 
                 Column("Destination_ID", Integer, primary_key=True),
                 Column("Folder_ID", Integer),
                 Column("MailConfiguration_ID", Integer)
                 )

meta.create_all(engine)