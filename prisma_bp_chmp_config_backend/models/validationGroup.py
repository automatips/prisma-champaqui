from sqlalchemy import *
from persistence.db import meta, engine

validationGroups = Table("ValidationGroups", meta, 
                 Column("ValidationGroup_ID", Integer, primary_key=True),
                 Column("Name", Text)
                 )

meta.create_all(engine)