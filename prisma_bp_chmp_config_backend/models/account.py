from sqlalchemy import *
from persistence.db import meta, engine

accounts = Table("Accounts", meta, 
                 Column("Account_ID", Integer, primary_key=True),
                 Column("FIID", Text),
                 Column("FIID_Padre", Integer)
                 )

meta.create_all(engine)