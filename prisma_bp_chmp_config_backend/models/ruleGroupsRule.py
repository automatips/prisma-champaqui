from sqlalchemy import *
from persistence.db import meta, engine

ruleGroupsRules = Table("RuleGroupsRules", meta, 
                 Column("RuleGroupsRules_ID", Integer, primary_key=True),
                 Column("RuleGroup_ID", Integer),
                 Column("Rule_Id", Integer),
                 Column("Order", Text)
                 )

meta.create_all(engine)