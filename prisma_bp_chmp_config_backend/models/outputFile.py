from sqlalchemy import *
from persistence.db import meta, engine

outputFiles = Table("OutputFiles", meta, 
                 Column("OutputFile_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("FIID", Integer),
                 Column("OriginalName", Integer),
                 Column("FileBasename", Integer),
                 Column("DateFormat_ID", Integer),
                 Column("FileExtension", Integer),
                 Column("Refresh_ID", Integer),
                 Column("Design_ID", Integer),
                 Column("ProcessType_ID", Integer),
                 Column("Encodding", Integer),
                 Column("ShouldEncrypt", Integer),
                 Column("ShouldZip", Integer)
                 )

meta.create_all(engine)