from sqlalchemy import *
from persistence.db import meta, engine

emailTemplates = Table("EmailTemplates", meta, 
                 Column("EmailTemplate_ID", Integer, primary_key=True),
                 Column("TemplateName", Text),
                 Column("EmailSubject", Text),
                 Column("EmailBodyTemplate", Text)
                 )

meta.create_all(engine)