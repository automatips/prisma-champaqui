from sqlalchemy import *
from persistence.db import meta, engine

inputFiles = Table("InputFiles", meta, 
                 Column("InputFile_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("FIID", Integer),
                 Column("Basename", Integer),
                 Column("Refresh_ID", Integer),
                 Column("Design_ID", Integer),
                 Column("ProcessType_ID", Integer),
                 Column("DateFormat_ID", Integer),
                 Column("ValidationRulesGroup_ID", Integer),
                 Column("Encodding", Integer),
                 Column("CanEncrypt", Integer),
                 Column("CanZip", Integer),
                 Column("CanPadding", Integer),
                 Column("Frequency_ID", Integer),
                 Column("Rubro_ID", Integer),
                 Column("Should_Notify_Delay", Integer),
                 Column("Waiting_To_Notify_Days", Integer)
                 )

meta.create_all(engine)