from sqlalchemy import *
from persistence.db import meta, engine

processTypes = Table("ProcessTypes", meta, 
                 Column("ProcessType_ID", Integer, primary_key=True),
                 Column("ProcessType_Name", Text)
                 )

meta.create_all(engine)