from sqlalchemy import *
from persistence.db import meta, engine

servers = Table("Servers", meta, 
                 Column("Server_ID", String(32), primary_key=True),
                 Column("ServerName", Text),
                 Column("ServerIP", Text)
                 )

meta.create_all(engine)