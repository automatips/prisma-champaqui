from sqlalchemy import *
from persistence.db import meta, engine

frequencies = Table("Frequencies", meta, 
                 Column("Frequency_ID", Integer, primary_key=True),
                 Column("Frequency_Name", Text)
                 )

meta.create_all(engine)