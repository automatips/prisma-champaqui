from sqlalchemy import *
from persistence.db import meta, engine

validationRulesGroups = Table("ValidationRulesGroups", meta, 
                 Column("ValidationGroup_ID", Integer, primary_key=True),
                 Column("ValidationRule_ID", Integer),
                 Column("Order", Integer)
                 )

meta.create_all(engine)