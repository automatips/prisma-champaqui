from sqlalchemy import *
from persistence.db import meta, engine

allowedFilesFolders = Table("AllowedFilesFolders", meta, 
                 Column("AllowedFilesFolders_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("Folder_ID", Integer),
                 Column("AllowedFile_ID", Integer),
                 Column("InputFile_ID", Integer),
                 Column("OutputFile_ID", Integer)
                 )

meta.create_all(engine)