from sqlalchemy import *
from persistence.db import meta, engine

accountGroups = Table("AccountGroups", meta, 
                 Column("AccountGroup_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("AccountEmailGroup_ID", Integer),
                 Column("Email_ID", Integer),
                 Column("AccountGroupName", Text)
                 )

meta.create_all(engine)