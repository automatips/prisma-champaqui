from sqlalchemy import *
from persistence.db import meta, engine

accountFolders = Table("AccountFolders", meta, 
                 Column("AccountFolder_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("Folder_ID", Integer),
                 Column("Server_ID", Text),
                 Column("Path", Text),
                 Column("RuleGroup_ID", Integer)
                 )

meta.create_all(engine)