from sqlalchemy import *
from persistence.db import meta, engine

ruleGroupRuleParameters = Table("RuleGroupRuleParameters", meta, 
                 Column("RuleGroupRuleParameter_ID", Integer, primary_key=True),
                 Column("RuleGroup_ID", Integer),
                 Column("Rule_ID", Integer),
                 Column("Parameter_Order", Integer),
                 Column("Parameter_Name", Text),
                 Column("Parameter_Value", Text)
                 )

meta.create_all(engine)