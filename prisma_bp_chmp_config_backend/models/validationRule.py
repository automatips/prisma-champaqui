from sqlalchemy import *
from persistence.db import meta, engine

validationRules = Table("ValidationRules", meta, 
                 Column("ValidationRule_ID", Integer, primary_key=True),
                 Column("ActionType", Integer),
                 Column("URL", Text),
                 Column("Script", Text)
                 )

meta.create_all(engine)