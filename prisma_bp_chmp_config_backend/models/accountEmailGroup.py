from sqlalchemy import *
from persistence.db import meta, engine

accountEmailGroups = Table("AccountEmailGroups", meta,
                 Column("AccountEmailGroup_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("SubjectPrefix", Integer),
                 Column("EmailTemplate_ID", Integer)
                 )

meta.create_all(engine)