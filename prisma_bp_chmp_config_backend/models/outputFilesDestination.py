from sqlalchemy import *
from persistence.db import meta, engine

outputFilesDestinations = Table("OutputFilesDestinations", meta, 
                 Column("OutputFilesDestination_ID", Integer, primary_key=True),
                 Column("Account_ID", Integer),
                 Column("OutputFile_ID", Integer),
                 Column("Destination_ID", Integer)
                 )

meta.create_all(engine)