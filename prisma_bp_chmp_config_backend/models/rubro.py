from sqlalchemy import *
from persistence.db import meta, engine

rubros = Table("Rubros", meta, 
                 Column("Rubro_ID", Integer, primary_key=True),
                 Column("Rubro_Name", Text)
                 )

meta.create_all(engine)