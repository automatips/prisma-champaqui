from sqlalchemy import *
from persistence.db import meta, engine

ruleGroups = Table("RuleGroups", meta, 
                 Column("RuleGroup_ID", Integer, primary_key=True),
                 Column("Name", Text)
                 )

meta.create_all(engine)