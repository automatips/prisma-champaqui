from sqlalchemy import *
from persistence.db import meta, engine

folders = Table("Folders", meta, 
                 Column("Folder_ID", Integer, primary_key=True),
                 Column("Server_ID", Text),
                 Column("Path", Text)
                 )

meta.create_all(engine)