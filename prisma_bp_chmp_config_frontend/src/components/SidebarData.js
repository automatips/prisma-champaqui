import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [
  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Accounts',
    path: '/accounts',
    icon: <IoIcons.IoIosBook/>,
    cName: 'nav-text'
  },
  {
    title: 'Account Emails',
    path: '/accountemails',
    icon: <IoIcons.IoIosMail/>,
    cName: 'nav-text'
  },
  {
    title: 'AEG',
    path: '/accountemailgroups',
    icon: <IoIcons.IoIosAnalytics/>,
    cName: 'nav-text'
  },
  {
    title: 'Account Group',
    path: '/accountgroups',
    icon: <IoIcons.IoIosClipboard/>,
    cName: 'nav-text'
  },
  {
    title: 'Email templates',
    path: '/emailtemplates',
    icon: <IoIcons.IoIosAlbums/>,
    cName: 'nav-text'
  }
];
