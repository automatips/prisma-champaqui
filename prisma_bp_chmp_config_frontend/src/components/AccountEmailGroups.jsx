import { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap'
import axios from "axios";

const data = [];
const urlGetAccountEmailGroups="http://localhost:8000/accountemailgroups";

class AccountEmailGroupsCRUD extends Component {
  state = {
    data: data,
    form: {
        AccountEmailGroup_ID: '',
        Account_ID: '',
        SubjectPrefix: '',
        EmailTemplate_ID: ''
    },
    modalInsert: false,
    modalEdit: false,
  };

  getResults = () => {
    axios.get(urlGetAccountEmailGroups,{ crossdomain: true }).then(response=>{
      console.log('aca van:'+response.data);
      this.setState({data: response.data});
    })
  }

  componentDidMount(){
    this.getResults();
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    });
  }

  showModalInsert = () => {
    this.setState({ modalInsert: true});
  }

  hideModalInsert = () => {
    this.setState({ modalInsert: false});
  }

  showModalEdit = (record) => {
    this.setState({ modalEdit: true, form: record});
  }

  hideModalEdit = () => {
    this.setState({ modalEdit: false});
  }

  insert = () => {
    var newValue = { ...this.state.form };
    newValue.AccountEmailGroup_ID = this.state.data.length + 1;
    var list = this.state.data;
    list.push(newValue);
    this.setState({ data: list, modalInsert: false });
  }

  edit=(editAccountEmailGroup)=>{
    var contador=0;
    var list=this.state.data;
    // eslint-disable-next-line array-callback-return
    list.map((record)=>{
      if(editAccountEmailGroup.AccountEmailGroup_ID===record.AccountEmailGroup_ID){
        list[contador].Account_ID=editAccountEmailGroup.Account_ID;
        list[contador].SubjectPrefix=editAccountEmailGroup.SubjectPrefix;
        list[contador].EmailTemplate_ID=editAccountEmailGroup.EmailTemplate_ID;
      }
      contador++;
    });
    this.setState({data: list, modalEdit: false});
  }

  delete=(deleteAccountEmailGroup)=>{
    var opcion=window.confirm("do you really want to delete the record "+deleteAccountEmailGroup.AccountEmailGroup_ID);
      if(opcion){
        var contador=0;
        var list = this.state.data;
        // eslint-disable-next-line array-callback-return
        list.map((record)=>{
          if(record.AccountEmailGroup_ID===deleteAccountEmailGroup.AccountEmailGroup_ID){
            list.splice(contador, 1);
          }
          contador++;
        });
        this.setState({data: list});
      }
    }

  render() {
    return (
      <>          
        <Container>
          <br />
          <Button color="success" onClick={() => this.showModalInsert()}>Insert new Account Email Group</Button>
          <br />
          <br />

          <Table>
            <thead>
              <tr>
                <th>AccountEmailGroup_ID</th>
                <th>Account_ID</th>
                <th>Subject Prefix</th>
                <th>Email Template ID</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((rowElement) => (
                <tr>
                  <td>{rowElement.AccountEmailGroup_ID}</td>
                  <td>{rowElement.Account_ID}</td>
                  <td>{rowElement.SubjectPrefix}</td>
                  <td>{rowElement.EmailTemplate_ID}</td>
                  <td><Button color="primary" onClick={() => this.showModalEdit(rowElement)}>Edit</Button>{"  "}
                    <Button color="danger" onClick={() => this.delete(rowElement)}>Delete</Button></td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalInsert}>
          <ModalHeader>
            <div>
              <h3>Insert Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account Email Group ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </FormGroup>

            <FormGroup>
              <label>Account_ID:</label>
              <input className="form-control" name="Account_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Subject Prefix:</label>
              <input className="form-control" name="SubjectPrefix" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Email Template ID:</label>
              <input className="form-control" name="EmailTemplate_ID" type="text" onChange={this.handleChange} />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.insert()} >Save</Button>
            <Button color="danger" onClick={() => this.hideModalInsert()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEdit}>
          <ModalHeader>
            <div>
              <h3>Edit Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account Email Group ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.Account_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Account_ID:</label>
              <input className="form-control" name="Account_ID" type="text" onChange={this.handleChange} value={this.state.form.Account_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Subject Prefix:</label>
              <input className="form-control" name="SubjectPrefix" type="text" onChange={this.handleChange} value={this.state.form.SubjectPrefix}/>
            </FormGroup>

            <FormGroup>
              <label>Email Template ID:</label>
              <input className="form-control" name="EmailTemplate_ID" type="text" onChange={this.handleChange} value={this.state.form.EmailTemplate_ID}/>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.edit(this.state.form)}>Save</Button>
            <Button color="danger"  onClick={() => this.hideModalEdit()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

export default AccountEmailGroupsCRUD;