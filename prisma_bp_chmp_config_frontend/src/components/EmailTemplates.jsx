import { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap'
import axios from "axios";

const data = [];
const urlGetEmailTemplates="http://localhost:8000/emailtemplates";

class EmailTemplatesCRUD extends Component {
  state = {
    data: data,
    form: {
        EmailTemplate_ID: '',
        TemplateName: '',
        EmailSubject: '',
        EmailBodyTemplate: '',
    },
    modalInsert: false,
    modalEdit: false,
  };

  getResults = () => {
    axios.get(urlGetEmailTemplates,{ crossdomain: true }).then(response=>{
      console.log('aca van:'+response.data);
      this.setState({data: response.data});
    })
  }

  componentDidMount(){
    this.getResults();
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    });
  }

  showModalInsert = () => {
    this.setState({ modalInsert: true});
  }

  hideModalInsert = () => {
    this.setState({ modalInsert: false});
  }

  showModalEdit = (record) => {
    this.setState({ modalEdit: true, form: record});
  }

  hideModalEdit = () => {
    this.setState({ modalEdit: false});
  }

  insert = () => {
    var newValue = { ...this.state.form };
    newValue.EmailTemplate_ID = this.state.data.length + 1;
    var list = this.state.data;
    list.push(newValue);
    this.setState({ data: list, modalInsert: false });
  }

  edit=(editEmailTemplates)=>{
    var contador=0;
    var list=this.state.data;
    // eslint-disable-next-line array-callback-return
    list.map((record)=>{
      if(editEmailTemplates.EmailTemplate_ID===record.EmailTemplate_ID){
        list[contador].TemplateName=editEmailTemplates.TemplateName;
        list[contador].EmailSubject=editEmailTemplates.EmailSubject;
        list[contador]. EmailBodyTemplate=editEmailTemplates.EmailBodyTemplate;
      }
      contador++;
    });
    this.setState({data: list, modalEdit: false});
  }

  delete=(deleteEmailTemplate)=>{
    var opcion=window.confirm("do you really want to delete the record "+deleteEmailTemplate.EmailTemplate_ID);
      if(opcion){
        var contador=0;
        var list = this.state.data;
        // eslint-disable-next-line array-callback-return
        list.map((record)=>{
          if(record.EmailTemplate_ID===deleteEmailTemplate.EmailTemplate_ID){
            list.splice(contador, 1);
          }
          contador++;
        });
        this.setState({data: list});
      }
    }

  render() {
    return (
      <>          
        <Container>
          <br />
          <Button color="success" onClick={() => this.showModalInsert()}>Insert new Email Template</Button>
          <br />
          <br />

          <Table>
            <thead>
              <tr>
                <th>EmailTemplate_ID</th>
                <th>Template Name</th>
                <th>Email Subject</th>
                <th>Email Body Template</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((rowElement) => (
                <tr>
                  <td>{rowElement.EmailTemplate_ID}</td>
                  <td>{rowElement.TemplateName}</td>
                  <td>{rowElement.EmailSubject}</td>
                  <td>{rowElement.EmailBodyTemplate}</td>
                  <td><Button color="primary" onClick={() => this.showModalEdit(rowElement)}>Edit</Button>{"  "}
                    <Button color="danger" onClick={() => this.delete(rowElement)}>Delete</Button></td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalInsert}>
          <ModalHeader>
            <div>
              <h3>Insert Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Email Template ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </FormGroup>

            <FormGroup>
              <label>Template Name:</label>
              <input className="form-control" name="TemplateName" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Email Subject:</label>
              <input className="form-control" name="EmailSubject" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Email Body Template:</label>
              <input className="form-control" name="EmailBodyTemplate" type="text" onChange={this.handleChange} />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.insert()} >Save</Button>
            <Button color="danger" onClick={() => this.hideModalInsert()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEdit}>
          <ModalHeader>
            <div>
              <h3>Edit Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Email Template ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.EmailTemplate_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Template Name:</label>
              <input className="form-control" name="TemplateName" type="text" onChange={this.handleChange} value={this.state.form.TemplateName}/>
            </FormGroup>

            <FormGroup>
              <label>Email Subject:</label>
              <input className="form-control" name="EmailSubject" type="text" onChange={this.handleChange} value={this.state.form.EmailSubject}/>
            </FormGroup>

            <FormGroup>
              <label>Email Body Template:</label>
              <input className="form-control" name="EmailBodyTemplate" type="text" onChange={this.handleChange} value={this.state.form.EmailBodyTemplate}/>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.edit(this.state.form)}>Save</Button>
            <Button color="danger"  onClick={() => this.hideModalEdit()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

export default EmailTemplatesCRUD;