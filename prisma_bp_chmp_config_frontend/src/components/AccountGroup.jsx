import { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap'
import axios from "axios";

const data = [];
const urlGetAccountGroup="http://localhost:8000/accountgroups";

class AccountGroupCRUD extends Component {
  state = {
    data: data,
    form: {
        AccountGroup_ID: '',
        Account_ID: '',
        AccountEmailGroup_ID: '',
        Email_ID: '',
        AccountGroupName: ''
    },
    modalInsert: false,
    modalEdit: false,
  };

  getResults = () => {
    axios.get(urlGetAccountGroup,{ crossdomain: true }).then(response=>{
      console.log('aca van:'+response.data);
      this.setState({data: response.data});
    })
  }

  componentDidMount(){
    this.getResults();
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    });
  }

  showModalInsert = () => {
    this.setState({ modalInsert: true});
  }

  hideModalInsert = () => {
    this.setState({ modalInsert: false});
  }

  showModalEdit = (record) => {
    this.setState({ modalEdit: true, form: record});
  }

  hideModalEdit = () => {
    this.setState({ modalEdit: false});
  }

  insert = () => {
    var newValue = { ...this.state.form };
    newValue.AccountGroup_ID = this.state.data.length + 1;
    var list = this.state.data;
    list.push(newValue);
    this.setState({ data: list, modalInsert: false });
  }

  edit=(editAccountGroup)=>{
    var contador=0;
    var list=this.state.data;
    // eslint-disable-next-line array-callback-return
    list.map((record)=>{
      if(editAccountGroup.AccountGroup_ID===record.AccountGroup_ID){
        list[contador].Account_ID=editAccountGroup.Account_ID;
        list[contador].AccountEmailGroup_ID=editAccountGroup.AccountEmailGroup_ID;
        list[contador].Email_ID=editAccountGroup.Email_ID;
        list[contador].AccountGroupName=editAccountGroup.AccountGroupName;
      }
      contador++;
    });
    this.setState({data: list, modalEdit: false});
  }

  delete=(deleteAccountGroup)=>{
    var opcion=window.confirm("do you really want to delete the record "+deleteAccountGroup.AccountEmail_ID);
      if(opcion){
        var contador=0;
        var list = this.state.data;
        // eslint-disable-next-line array-callback-return
        list.map((record)=>{
          if(record.AccountGroup_ID===deleteAccountGroup.AccountGroup_ID){
            list.splice(contador, 1);
          }
          contador++;
        });
        this.setState({data: list});
      }
    }

  render() {
    return (
      <>          
        <Container>
          <br />
          <Button color="success" onClick={() => this.showModalInsert()}>Insert new Account Group</Button>
          <br />
          <br />

          <Table>
            <thead>
              <tr>
                <th>Account Group ID</th>
                <th>Account ID</th>
                <th>Account Email Group ID</th>
                <th>Email ID</th>
                <th>Account Group Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((rowElement) => (
                <tr>
                  <td>{rowElement.AccountGroup_ID}</td>
                  <td>{rowElement.Account_ID}</td>
                  <td>{rowElement.AccountEmailGroup_ID}</td>
                  <td>{rowElement.Email_ID}</td>
                  <td>{rowElement.AccountGroupName}</td>
                  <td><Button color="primary" onClick={() => this.showModalEdit(rowElement)}>Edit</Button>{"  "}
                    <Button color="danger" onClick={() => this.delete(rowElement)}>Delete</Button></td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalInsert}>
          <ModalHeader>
            <div>
              <h3>Insert Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account Group ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </FormGroup>

            <FormGroup>
              <label>Account ID:</label>
              <input className="form-control" name="Account_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Account Email Group ID:</label>
              <input className="form-control" name="AccountEmailGroup_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Email ID:</label>
              <input className="form-control" name="Email_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Account Group Name:</label>
              <input className="form-control" name="AccountGroupName" type="text" onChange={this.handleChange} />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.insert()} >Save</Button>
            <Button color="danger" onClick={() => this.hideModalInsert()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEdit}>
          <ModalHeader>
            <div>
              <h3>Edit Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account Email ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.AccountEmail_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Account ID:</label>
              <input className="form-control" name="Account_ID" type="text" onChange={this.handleChange} value={this.state.form.Account_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Account Email Group ID:</label>
              <input className="form-control" name="AccountEmailGroup_ID" type="text" onChange={this.handleChange} value={this.state.form.AccountEmailGroup_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Email ID:</label>
              <input className="form-control" name="Email_ID" type="text" onChange={this.handleChange} value={this.state.form.Email_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Account Group Name:</label>
              <input className="form-control" name="MailName" type="text" onChange={this.handleChange} value={this.state.form.MailName}/>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.edit(this.state.form)}>Save</Button>
            <Button color="danger"  onClick={() => this.hideModalEdit()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

export default AccountGroupCRUD;