import { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap'
import axios from "axios";

const data = [];
const urlGetAccountEmails="http://localhost:8000/accountemails";

class AccountEmailCRUD extends Component {
  state = {
    data: data,
    form: {
        AccountEmail_ID: '',
        Account_ID: '',
        AccountEmailGroup_ID: '',
        Email_ID: '',
        MailName: '',
        MailAdress: ''
    },
    modalInsert: false,
    modalEdit: false,
  };

  getResults = () => {
    axios.get(urlGetAccountEmails,{ crossdomain: true }).then(response=>{
      console.log('aca van:'+response.data);
      this.setState({data: response.data});
    })
  }

  componentDidMount(){
    this.getResults();
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    });
  }

  showModalInsert = () => {
    this.setState({ modalInsert: true});
  }

  hideModalInsert = () => {
    this.setState({ modalInsert: false});
  }

  showModalEdit = (record) => {
    this.setState({ modalEdit: true, form: record});
  }

  hideModalEdit = () => {
    this.setState({ modalEdit: false});
  }

  insert = () => {
    var newValue = { ...this.state.form };
    newValue.AccountEmail_ID = this.state.data.length + 1;
    var list = this.state.data;
    list.push(newValue);
    this.setState({ data: list, modalInsert: false });
  }

  edit=(editAccountEmail)=>{
    var contador=0;
    var list=this.state.data;
    // eslint-disable-next-line array-callback-return
    list.map((record)=>{
      if(editAccountEmail.AccountEmail_ID===record.AccountEmail_ID){
        list[contador].Account_ID=editAccountEmail.Account_ID;
        list[contador].AccountEmailGroup_ID=editAccountEmail.AccountEmailGroup_ID;
        list[contador].Email_ID=editAccountEmail.Email_ID;
        list[contador].MailName=editAccountEmail.MailName;
        list[contador].MailAdress=editAccountEmail.MailAdress;
      }
      contador++;
    });
    this.setState({data: list, modalEdit: false});
  }

  delete=(deleteAccountEmail)=>{
    var opcion=window.confirm("do you really want to delete the record "+deleteAccountEmail.AccountEmail_ID);
      if(opcion){
        var contador=0;
        var list = this.state.data;
        // eslint-disable-next-line array-callback-return
        list.map((record)=>{
          if(record.AccountEmail_ID===deleteAccountEmail.AccountEmail_ID){
            list.splice(contador, 1);
          }
          contador++;
        });
        this.setState({data: list});
      }
    }

  render() {
    return (
      <>          
        <Container>
          <br />
          <Button color="success" onClick={() => this.showModalInsert()}>Insert new Account Email</Button>
          <br />
          <br />

          <Table>
            <thead>
              <tr>
                <th>AccountEmail_ID</th>
                <th>Account_ID</th>
                <th>AccountEmailGroup_ID</th>
                <th>Email_ID</th>
                <th>Mail Name</th>
                <th>Mail Adress</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((rowElement) => (
                <tr>
                  <td>{rowElement.AccountEmail_ID}</td>
                  <td>{rowElement.Account_ID}</td>
                  <td>{rowElement.AccountEmailGroup_ID}</td>
                  <td>{rowElement.Email_ID}</td>
                  <td>{rowElement.MailName}</td>
                  <td>{rowElement.MailAdress}</td>
                  <td><Button color="primary" onClick={() => this.showModalEdit(rowElement)}>Edit</Button>{"  "}
                    <Button color="danger" onClick={() => this.delete(rowElement)}>Delete</Button></td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalInsert}>
          <ModalHeader>
            <div>
              <h3>Insert Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account Email ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </FormGroup>

            <FormGroup>
              <label>Account ID:</label>
              <input className="form-control" name="Account_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Account EmailGroup ID:</label>
              <input className="form-control" name="AccountEmailGroup_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Email ID:</label>
              <input className="form-control" name="Email_ID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Mail Name:</label>
              <input className="form-control" name="MailName" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>Mail Adress:</label>
              <input className="form-control" name="MailAdress" type="text" onChange={this.handleChange} />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.insert()} >Save</Button>
            <Button color="danger" onClick={() => this.hideModalInsert()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEdit}>
          <ModalHeader>
            <div>
              <h3>Edit Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account Email ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.AccountEmail_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Account ID:</label>
              <input className="form-control" name="Account_ID" type="text" onChange={this.handleChange} value={this.state.form.Account_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Account Email Group ID:</label>
              <input className="form-control" name="AccountEmailGroup_ID" type="text" onChange={this.handleChange} value={this.state.form.AccountEmailGroup_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Email ID:</label>
              <input className="form-control" name="Email_ID" type="text" onChange={this.handleChange} value={this.state.form.Email_ID}/>
            </FormGroup>

            <FormGroup>
              <label>Mail Name:</label>
              <input className="form-control" name="MailName" type="text" onChange={this.handleChange} value={this.state.form.MailName}/>
            </FormGroup>

            <FormGroup>
              <label>Mail Adress:</label>
              <input className="form-control" name="MailAdress" type="text" onChange={this.handleChange} value={this.state.form.MailAdress}/>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.edit(this.state.form)}>Save</Button>
            <Button color="danger"  onClick={() => this.hideModalEdit()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

export default AccountEmailCRUD;