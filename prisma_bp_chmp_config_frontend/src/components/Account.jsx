import { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap'
import axios from "axios";
import * as IoIcons from 'react-icons/io';

const data = [];
const urlGetAccounts="http://localhost:8000/accounts";

class AccountCRUD extends Component {
  state = {
    data: data,
    form: {
        Account_ID: '',
        FIID: '',
        FIID_Padre: ''
    },
    modalInsert: false,
    modalEdit: false,
  };

  getResults = () => {
    axios.get(urlGetAccounts,{ crossdomain: true }).then(response=>{
      console.log('aca van:'+response.data);
      this.setState({data: response.data});
    })
  }

  componentDidMount(){
    this.getResults();
  }

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
    });
  }

  showModalInsert = () => {
    this.setState({ modalInsert: true});
  }

  hideModalInsert = () => {
    this.setState({ modalInsert: false});
  }

  showModalEdit = (record) => {
    this.setState({ modalEdit: true, form: record});
  }

  hideModalEdit = () => {
    this.setState({ modalEdit: false});
  }

  insert = () => {
    var newValue = { ...this.state.form };
    newValue.Account_ID = this.state.data.length + 1;
    var list = this.state.data;
    list.push(newValue);
    this.setState({ data: list, modalInsert: false });
  }

  edit=(editAccount)=>{
    var contador=0;
    var list=this.state.data;
    // eslint-disable-next-line array-callback-return
    list.map((record)=>{
      if(editAccount.Account_ID===record.Account_ID){
        list[contador].FIID=editAccount.FIID;
        list[contador].FIID_Padre=editAccount.FIID_Padre;
      }
      contador++;
    });
    this.setState({data: list, modalEdit: false});
  }

  delete=(deleteAccount)=>{
    var opcion=window.confirm("do you really want to delete the record "+deleteAccount.Account_ID);
      if(opcion){
        var contador=0;
        var list = this.state.data;
        // eslint-disable-next-line array-callback-return
        list.map((record)=>{
          if(record.Account_ID===deleteAccount.Account_ID){
            list.splice(contador, 1);
          }
          contador++;
        });
        this.setState({data: list});
      }
    }

    goToEmail = () => {
      
    }

  render() {
    return (
      <>          
        <Container>
          <br />
          <Button color="success" onClick={() => this.showModalInsert()}>Insert new Account</Button>
          <br />
          <br />

          <Table>
            <thead>
              <tr>
                <th>Account_ID</th>
                <th>FIID</th>
                <th>FIID_Padre</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((rowElement) => (
                <tr>
                  <td>{rowElement.Account_ID}</td>
                  <td>{rowElement.FIID}</td>
                  <td>{rowElement.FIID_Padre}</td>
                  <td>
                      <Button color="primary" onClick={() => this.showModalEdit(rowElement)}>Edit</Button>{"  "}
                      <Button color="danger" onClick={() => this.delete(rowElement)}>Delete</Button>
                      <Button color="warning"  onClick={() => this.goToEmail()}><IoIcons.IoIosMail/>Emails</Button>  
                  </td>
                    
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalInsert}>
          <ModalHeader>
            <div>
              <h3>Insert Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account_ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </FormGroup>

            <FormGroup>
              <label>FIID:</label>
              <input className="form-control" name="FIID" type="text" onChange={this.handleChange} />
            </FormGroup>

            <FormGroup>
              <label>FIID_Padre:</label>
              <input className="form-control" name="FIID_Padre" type="text" onChange={this.handleChange} />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.insert()} >Save</Button>
            <Button color="danger" onClick={() => this.hideModalInsert()}>Cancel</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modalEdit}>
          <ModalHeader>
            <div>
              <h3>Edit Record</h3>
            </div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>Account_ID:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.Account_ID}/>
            </FormGroup>

            <FormGroup>
              <label>FIID:</label>
              <input className="form-control" name="FIID" type="text" onChange={this.handleChange} value={this.state.form.FIID}/>
            </FormGroup>

            <FormGroup>
              <label>FIID_Padre:</label>
              <input className="form-control" name="FIID_Padre" type="text" onChange={this.handleChange} value={this.state.form.FIID_Padre}/>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" onClick={() => this.edit(this.state.form)}>Save</Button>
            <Button color="danger"  onClick={() => this.hideModalEdit()}>Cancel</Button>
            <Button color="warning"  onClick={() => this.goToEmail()}><IoIcons.IoIosMail/>Emails</Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}

export default AccountCRUD;