import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import AccountCRUD from './components/Account';
import AccountGroupCRUD from './components/AccountGroup';
import AccountEmailCRUD from './components/AccountEmail';
import AccountEmailGroupsCRUD from './components/AccountEmailGroups';
import EmailTemplatesCRUD from './components/EmailTemplates';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import imgPrisma from './imgs/backround.jpeg';

function App() {
  return (
    <>
      <div
        class="bg_image"
        style={{
          backgroundImage: `url(${imgPrisma})`,
          backgroundSize: "cover",
          height: "100vh",
          backgroundPosition: 'center',
          backgroundRepeat: 'no-repeat'
        }}
      >
        <Router>
          <Navbar />
          <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/accounts' component={AccountCRUD} />
                <Route path='/accountemails' component={AccountEmailCRUD} />
                <Route path='/accountemailgroups' component={AccountEmailGroupsCRUD} />
                <Route path='/emailtemplates' component={EmailTemplatesCRUD} />
                <Route path='/accountgroups' component={AccountGroupCRUD} />
          </Switch>
        </Router>
      </div>

    </>
  );
}

export default App;
